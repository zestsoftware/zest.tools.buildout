#!/bin/bash
# For python2.7 - python3 needs addtional share and local removal
rm -rf bin include lib .Python
virtualenv -p python2.7 .
bin/pip install --upgrade -r requirements.txt
