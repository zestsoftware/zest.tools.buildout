Zest tools buildout
===================

This is a bundle of various development tools collected in a buildout.
So things like paster, i18ndude, zest.releaser.  The idea is that you
add the bin directory to your PATH so that these tools are always
available.  Just run the usual:

  $ python2.7 bootstrap.py
  $ bin/buildout


Local egg proxy
---------------

One of the added tools is a local egg proxy, using
collective.eggproxy.  This will use pypi.zestsoftware.nl as upstream
egg server, which in turn takes the official pypi.python.org as
upstream egg server.  To use it:

- Run bin/eggproxy_run and see if that works.

- Add index = http://127.0.0.1:8888 to your ~/.buildout/default.cfg

- add something like this to your crontab:
  @reboot $HOME/buildout/tools/bin/eggproxy_run

When you are in a train or a plane where you know you have no internet
connection, this should work as well, but if you do not need any new
packages you can speed things up by editing etc/eggproxy.conf.in to
say always_refresh = 0, kill the bin/eggproxy_run process, rerun
bin/buildout and start bin/eggproxy_run again.

Alternatively you can decide not to run your own egg proxy but just go
through pypi.zestsoftware.nl directly by adding this to your
~/.buildout/default.cfg:

  index = http://pypi.zestsoftware.nl


Plans
-----

- Add svngrep, etc.
