History of the Zest tools buildout
==================================

0.2 (unreleased)
----------------

- Use latest versions of Fabric 1.14.1 dependencies. [fred]

- Update setuptools/zc.buildout. [fred]

- Added plonecli.  [maurits]

- Added zestreleaser.towncrier.  [maurits]

- Updated lots of packages and removed unused ones.  [maurits]

- Removed unused zptlint and pinned i18ndude 5.0.1.  [maurits]

- zest.releaser 6.12.5, check-manifest 0.35.  [maurits]

- i18ndude 4.3  [maurits]

- zest.releaser 6.10 [fred]

- bobtemplates.plone 1.0.5 [fred]

- zest.releaser 6.6.5.  [maurits]

- Added pycodestyle, which is the new name of the pep8 tool.  [maurits]

- Update zc.buildout and setuptools. Add requirements.txt for these [fred]

- Use latest bobtemplates.plone [fred]

- Renamed autopep8zest to autopep8medium.  [maurits]

- pyroma 2.0.0.  [maurits]

- Added isort command line tool.  [maurits]

- Updated lots of versions.  [maurits]

- Added plone.versioncheck.  Use the versioncheck script to check your
  versions.  Nicer than the checkversions script.  [maurits]

- i18ndude = 4.0.1
  [maurits]

- zest.releaser = 6.2
  [maurits]

- Add flake8, which combines pyflakes, pep8 and mccabe.
  See https://flake8.readthedocs.org/
  Suggestion is to integrate flake8 in your editor,
  or run it before committing.  Do not let it (or pyflakes) loose on
  scripts in a skins directory though, because it will break stuff.
  You can put these lines in ~/.config/flake8:

    [flake8]
    exclude = skins

  [maurits]

- Removed compass directory from buildout.  We use compass.cfg with
  rubygemsrecipe now, if we do not have something better in the
  project buildout.  Also, see MPI intranet and website for a compass
  section that uses an older compass and sass, if you have a project
  that has sass syntax that gives compile errors on newer versions.

- New or improved scripts: autopep8safe, autopep8full, and
  autopep8zest.
  [maurits]

- Added restview.  Alternative to longtest from zest.releaser, which
  should still be fine.
  [maurits]

- Update zope.tal and i18ndude to latest versions.
  [fred]

- Addition of zest.iconize
  [diederik]

- New pyflakes and pep8.
  [maurits]

- checkoutmanager 2.0, with parallel support.
  [maurits]

- zest.releaser 5.0, with Python 3 support.
  [maurits]

- Create bin/gensecret, mostly for Django SECRET_KEY.
  [maurits]

- Add mr.bob with bobtemplates.plone.
  [fred]

- zest.releaser 4.0.
  [maurits]

- Add coloroma, twine and wheel, in preparation for upcoming
  zest.releaser 4.0.
  [maurits]

- Update sass/compass in compass.cfg to latest 3.4.9.
  [fred]

- mr.developer -> 1.31 , pep8 -> 1.5.7, virtualenv -> 1.11.6. Fabric -> 1.10.0
  [fred]

- docutils 0.10 -> 0.12.
  [maurits]

- Add compass as a buildout recipe instead of running a manual install.sh. To
  use, extend compass.cfg in your buildout.cfg
  [fred]

- Revert from pyflakes 0.8.1 to 0.7.3.  The newer one crashes on
  python skin scripts.
  [maurits]

- Create autopep8zest script in bin, with proper default settings.
  [maurits]

- Add autopep8.  Wow.  Update to latest pep8 and pyflakes versions.
  [maurits]

- Add compass directory for compiling sass to css.  Usage:
  `compass/install.sh` to install it, `compass/compass.sh` to use it.
  [maurits]

- Fresh zc.buildout and friends.
  [maurits]

- Remove zest.stabilizer.  That was only needed for our old buildout
  setup with unstable.cfg and stable.cfg.
  [maurits]

- Make it clear that csscheck is not used by default.
  [maurits]

- Use http for mr.developer fetch for anonymous zest.tools.buildout users
  [fred]

- Use virtualenv 1.10.1
  [fred]

- Add Fabric 1.8.0.
  [maurits]

- zest.releaser 3.49
  [maurits]

- i18ndude 3.3.3.
  [maurits]

- Update pyflakes from 0.5 to 0.7.3.
  [maurits]

- New Theming package in ZestSkel
  [thom]

- zest.pocompile 1.4.
  [maurits]

- Update sources to new locations:
  Moved to github: zest.pocompile and zest.stabilizer.
  Moved to bitbucket: ZestSkel.
  [maurits]

- Use latest mr.developer 1.25.
  [maurits]


0.1 (2013-07-05)
----------------

- Moved to bitbucket.
  [maurits]

- zest.releaser 3.46.
  [maurits]

- Added zopeskel.diazotheme to the buildout. [jladage]

- check-manifest 0.12 no longer bothers us when releasing a buildout.
  [maurits]

- Added check-manifest and pyroma, which are called during the
  prerelease stage of zest.releaser.
  [maurits]

- zest.releaser = 3.44
  [maurits]

- setuptools-hg = 0.4
  [maurits]

- setuptools-git = 1.0b1
  [maurits]

- setuptools-subversion = 3.1
  [maurits]

- zest.releaser = 3.37
  [maurits]

- Big pep8 update: from 0.6.1 to 1.3.3.
  [maurits]

- Add z3c.dependencychecker.
  [maurits]

- zest.releaser 3.36
  [maurits]

- distribute 0.6.27.
  [maurits]

- zest.releaser 3.35.
  [maurits]

- zest.releaser 3.34.
  checkoutmanager 1.11
  [maurits]

- mr.developer = 1.20
  [maurits]

- zest.releaser = 3.31.
  [maurits]

- buildout.dumppickedversions 0.5 . Put parts on one line each.
  [fred]

- zest.releaser = 3.30.
  [maurits]

- zest.pocompile = 1.3
  [maurits]

- Various version updates that should be safe:
  PasteScript = 1.7.5
  Unidecode = 0.04.9
  collective.recipe.template = 1.9
  distribute = 0.6.24
  gocept.zestreleaser.customupload = 1.1.0
  jarn.mkrelease = 3.5
  mr.developer = 1.19
  mr.igor = 1.3
  py = 1.4.5
  pyflakes = 0.5
  python-gettext = 1.2
  setuptools-hg = 0.3
  virtualenv = 1.7
  [maurits]

- zest.releaser 3.28
  [maurits]

- gocept.zestreleaser.customupload = 1.0.3.
  [maurits]

- setuptools_hg 0.2.2, setuptools_subversion 1.6 (not used by default)
  [maurits]

- checkoutmanager = 1.9 (support forsvn upgrade command).
  [maurits]

- Use zest.releaser 3.26.
  [maurits]

- zest.releaser has been moved to github.
  [maurits]

- checkoutmanager 1.8.
  [maurits]

- ZopeSkel 2.21.2.
  [maurits]

- Downgraded zc.buildout to 1.4.4 and zc.recipe.egg to 1.2.2.
  [maurits]

- zest.releaser = 3.23
  [maurits]

- Update zopeskel.dexterity to 1.3b3
  [fred]

- Move buildout.cfg to tools.cfg and add a builout.cfg.in which extends on
  tools.cfg so that you can add local modifications in your local buildout.cfg
  [fred]

- Use lots of new versions (mostly what bin/checkversions suggests).
  [maurits]

- Added zest.pocompile to compile po files to mo files right before
  creating the source dist with zest.releaser.
  [maurits]

- Added zopeskel.dexterity for dexterity content type scaffolding.
  [fred]

- Added git-svn-helpers to have the gitify command.
  [mark]

- Added checkoutmanager.  See
  http://reinout.vanrees.org/weblog/2010/08/02/checkoutmanager.html
  [maurits]

- Use newer versions:
  distribute = 0.6.14
  eolfixer = 1.1
  pep8 = 0.5
  py = 1.3.3
  virtualenv = 1.4.9
  [maurits]

- Added z3c.checkversions, which adds bin/checkversions which you can
  use to report newer versions that are available on pypi.
  [maurits]

- Added gocept.zestreleaser.customupload = 1.0.2.  This offers
  uploading to a private pypi using scp, if you add something like
  this in your ~/.pypirc:
    [gocept.zestreleaser.customupload]
    ren = tgz.zestsoftware.nl:public_html/eggs
    plonehrm = pypi.zestsoftware.nl:/home/pypi/public_html/private
  [maurits]

- zest.releaser 3.12.
  [maurits]

- Pin mr.developer to 1.9 to avoid issues with older subversion
  clients (1.4).
  [maurits]

- Added pyflakes.
  [maurits]

- Use fresh zest.releaser 3.5, which searches for CHANGES.txt and
  HISTORY.txt in a better order.
  [maurits]

- Use fresh zest.releaser 3.4; generated zip files to avoid possible
  tarball problems with python2.4 (patch by Patrick Gerken).
  [maurits]

- Pin ZopeSkel to latest 2.15, with fixes from the BBQ sprint.  [maurits]

- Pinned more eggs by using buildout.dumppickedversions. This was
  needed especially after adding zptlint. [mark]

- Added zptlint. [mark]

- Added eolfixer by Reinout.
  [maurits]

- Use latest http://python-distribute.org/bootstrap.py.  Run
  'python2.4 bootstrap.py --distribute' to use distribute for this
  buildout.
  [maurits]

- Removed collective.dist from bin/zestpy, as the extra check,
  mregister and mupload commands for setup.py are not actually
  available, unless you call it with 'zestpy setup.py', which
  e.g. zest.releaser does not do.
  [maurits]

- Added pep8 package, freshly made available on pypi by Reinout.
  [maurits]

- Let the local eggproxy use pypi.zestsoftware.nl as egg server (which
  in turn uses the default pypi as egg server).
  [maurits]

- Updated to zest.releaser = 2.8.  [maurits]

- Added collective.eggproxy so you can run your own egg proxy if you
  want.  See README.txt for an explanation.
  [maurits]

- Updated to zest.releaser = 2.7.  [maurits]

- Added bin/zestpy interpreter, with among other collective.dist in
  its path.  [maurits]

- Using the mr.developer buildout extension, to optionally use the
  source for zest.releaser, zest.stabilizer and ZestSkel.  Run
  'bin/develop help' for explanation.  ZestSkel is always checked out
  as development version.  If you run into troubles while running
  bin/buildout: throw away the buildout directory (or the
  .installed.cfg file) and start fresh.  [maurits]

- Created a development version of this, so it is easier to work on
  tools that we have made ourselves, like zest.releaser.  Currently
  this is already done for ZestSkel.

- Use mr.developer for that development version.  Inspired by
  plonenext.  See http://pypi.python.org/pypi/mr.developer

- Pinned to latest zest.releaser (2.0) and zest.stabilizer (1.4).
  [maurits]

- Started new project.  [maurits]
